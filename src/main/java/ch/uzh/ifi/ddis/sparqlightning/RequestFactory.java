package ch.uzh.ifi.ddis.sparqlightning;

/*
 * #%L
 * SPARQLightning
 * %%
 * Copyright (C) 2013 University of Zurich, Department of Informatics
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpRequestBase;

/**
 * <a href="http://www.w3.org/TR/2013/REC-sparql11-protocol-20130321/">SPARQL
 * Protocol</a>
 * 
 * @author Thomas Scharrenbach
 * @version 0.0.1
 * @since 0.0.1
 * 
 */
public interface RequestFactory {

	//
	//
	//


	
	public static final String RDF_GRAPH_STORE = "rdf-graph-store";

	public static final String PARAM_QUERY = "query";

	public static final String PARAM_UPDATE = "update";

	public static final String PARAM_DEFAULT_GRAPH_URI = "default-graph-uri";

	public static final String PARAM_NAMED_GRAPH_URI = "named-graph-uri";

	public static final String PARAM_USING_GRAPH_URI = "using-graph-uri";

	public static final String PARAM_USING_NAMED_GRAPH_URI = "using-named-graph-uri";

	public static final String CONTENT_TYPE_FORM_URL_ENCODED = "application/x-www-form-urlencoded";

	public static final String CONTENT_TYPE_SPARQL_UPDATE = "application/sparql-update";

	public static final String CONTENT_TYPE_SPARQL_QUERY = "application/sparql-query";

	public static final String ACCEPT_TYPE_ALL = "*/*";
	
	public static final String ACCEPT_TYPE_SPARQL_XML = "application/sparql-results+xml";
	
	public static final String ACCEPT_TYPE_JSON = "application/sparql-results+json";
	
	public static final String ACCEPT_TYPE_RDFXML = "application/rdf+xml";
	
	public static final String ACCEPT_TYPE_TURTLE = "text/turtle";
	
	/**
	 * 
	 * @param endpoint
	 * @param defaultGraphUri
	 * @param namedGraphUri
	 * @param query
	 * @return
	 * @throws URISyntaxException
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	HttpRequestBase createQueryRequest(String endpoint, String defaultGraphUri,
			String namedGraphUri, String query, List<String> acceptedMediaTypes)
			throws URISyntaxException, ClientProtocolException, IOException;

	HttpRequestBase createUpdateRequest(String endpoint,
			String usingDefaultGraphUri, String usingNamedGraphUri, String query)
			throws URISyntaxException, ClientProtocolException, IOException;

}
