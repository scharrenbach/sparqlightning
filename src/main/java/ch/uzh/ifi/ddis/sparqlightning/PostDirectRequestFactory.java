package ch.uzh.ifi.ddis.sparqlightning;

/*
 * #%L
 * SPARQLightning
 * %%
 * Copyright (C) 2013 University of Zurich, Department of Informatics
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */



import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.apache.http.HttpHeaders;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * Note: does currently not work with all versions of virtuoso.
 * </p>
 * 
 * @author Thomas Scharrenbach
 * @version 0.0.1
 * @since 0.0.1
 * 
 */
public class PostDirectRequestFactory implements RequestFactory {

	private static final Logger _log = LoggerFactory
			.getLogger(PostDirectRequestFactory.class);

	//
	//
	//

	public HttpPost createQueryRequest(String endpoint, String defaultGraphUri,
			String namedGraphUri, String query, List<String> acceptedMediaTypes) throws URISyntaxException,
			ClientProtocolException, IOException {

		final URIBuilder builder = new URIBuilder(endpoint);
		if (defaultGraphUri != null) {
			builder.setParameter(PARAM_DEFAULT_GRAPH_URI, defaultGraphUri);
		}
		if (namedGraphUri != null) {
			builder.setParameter(PARAM_NAMED_GRAPH_URI, namedGraphUri);
		}

		final HttpPost request = new HttpPost(builder.build());

		final StringEntity body = new StringEntity(query);

		request.setHeader(HttpHeaders.CONTENT_TYPE, CONTENT_TYPE_SPARQL_QUERY);
		request.addHeader(HttpHeaders.ACCEPT, ACCEPT_TYPE_TURTLE);
		request.addHeader(HttpHeaders.ACCEPT, "charset=utf-8");

		request.setEntity(body);

		_log.debug("Created request \"{}\"", request);

		return request;

	}

	public HttpRequestBase createUpdateRequest(String endpoint,
			String usingGraphUri, String usingNamedGraphUri, String update)
			throws URISyntaxException, ClientProtocolException, IOException {
		final URIBuilder builder = new URIBuilder(endpoint);
		if (usingGraphUri != null) {
			builder.setParameter(PARAM_USING_GRAPH_URI, usingGraphUri);
		}
		if (usingGraphUri != null) {
			builder.setParameter(PARAM_USING_NAMED_GRAPH_URI, usingNamedGraphUri);
		}

		final HttpPost request = new HttpPost(builder.build());

		final StringEntity body = new StringEntity("INSERT DATA { <a> <p> <b> }");
	
		request.setHeader(HttpHeaders.CONTENT_TYPE, CONTENT_TYPE_SPARQL_UPDATE);
		request.addHeader(HttpHeaders.ACCEPT, ACCEPT_TYPE_ALL);

		request.setEntity(body);

		_log.debug("Created request \"{}\"", request);

		return request;
	}
}
