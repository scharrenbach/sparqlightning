package ch.uzh.ifi.ddis.sparqlightning;

/*
 * #%L
 * SPARQLightning
 * %%
 * Copyright (C) 2013 University of Zurich, Department of Informatics
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.apache.http.HttpHeaders;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Thomas Scharrenbach
 * @version 0.0.1
 * @since 0.0.1
 * 
 */
public class GetRequestFactory implements RequestFactory {

	private static final Logger _log = LoggerFactory
			.getLogger(GetRequestFactory.class);

	//
	//
	//

	public HttpGet createQueryRequest(String endpoint, String defaultGraphUri,
			String namedGraphUri, String query, List<String> acceptedMediaTypes) throws URISyntaxException,
			ClientProtocolException, IOException {

		URIBuilder builder = new URIBuilder(endpoint);
		builder.setParameter(PARAM_QUERY, query);
		if (defaultGraphUri != null) {
			builder.setParameter(PARAM_DEFAULT_GRAPH_URI, defaultGraphUri);
		}
		if (namedGraphUri != null) {
			builder.setParameter(PARAM_NAMED_GRAPH_URI, namedGraphUri);
		}

		final HttpGet request = new HttpGet(builder.build());

		request.setHeader(HttpHeaders.ACCEPT, ACCEPT_TYPE_ALL);
		//request.addHeader(HttpHeaders.ACCEPT, "charset=utf-8");

		_log.debug("Created request {}", request);

		return request;

	}

	/**
	 * Not supported, as updates have to be requested via HTTP GET.
	 * 
	 * @throws UnsupportedOperationException
	 */
	public HttpRequestBase createUpdateRequest(String endpoint,
			String usingDefaultGraphUri, String usingNamedGraphUri, String query)
			throws URISyntaxException, ClientProtocolException, IOException {
		throw new UnsupportedOperationException(
				"Updates can only be performed via HTTP GET!");
	}
}
