package ch.uzh.ifi.ddis.sparqlightning;

/*
 * #%L
 * SPARQLightning
 * %%
 * Copyright (C) 2013 University of Zurich, Department of Informatics
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import net.minidev.json.JSONValue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * Implementation of parser for <a
 * href="http://www.w3.org/TR/sparql11-results-json/">SPARQL query results in
 * JSON format</a>.
 * </p>
 * 
 * @author Thomas Scharrenbach
 * @version 0.0.1
 * @since 0.0.1
 * 
 */
public class JSONResultSetParserImpl implements JSONResultSetParser {

	private static final Logger _log = LoggerFactory
			.getLogger(JSONResultSetParserImpl.class);

	/**
	 * Parameter types for
	 * <ul>
	 * <li>{@link #_handleUri(String, String, String, JSONVariableHandler)},</li>
	 * <li>{@link #_handleLiteral(String, String, String, JSONVariableHandler)},
	 * and</li>
	 * <li>{@link #_handleBNode(String, String, String, JSONVariableHandler)}.</li>
	 * </ul>
	 */
	private static final Class<?>[] parameterTypes = { String.class,
			String.class, String.class, Map.class };

	private static Method getMethod(String methodName) {
		try {
			return JSONResultSetParserImpl.class.getDeclaredMethod(methodName,
					parameterTypes);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * <p>
	 * Types to methods mapping for handling types via Java reflection.
	 * </p>
	 */
	@SuppressWarnings("serial")
	private static final Map<Object, Method> TYPES_MAP = Collections
			.unmodifiableMap(new HashMap<Object, Method>() {
				{
					put(TYPE_URI, getMethod("_handleUri"));
					put(TYPE_LITERAL, getMethod("_handleLiteral"));
					put(TYPE_BNODE, getMethod("_handleBNode"));
				}
			});

	/**
	 * Calls the JSON parser on the input stream and calls
	 * {@link #handleHead(JSONObject)} and {@link #handleResults(JSONObject)}.
	 * 
	 * @param in
	 * @throws IOException
	 */
	@Override
	public List<Map<String, VariableBinding>> parse(InputStream in)
			throws IOException {

		final JSONObject result = (JSONObject) JSONValue.parse(in);
		final LinkedList<Map<String, VariableBinding>> resultList = new LinkedList<Map<String, VariableBinding>>();
		handleHead((JSONObject) result.get(KEY_HEAD));
		handleResults((JSONObject) result.get(KEY_RESULTS), resultList);
		return resultList;
	}

	/**
	 * <p>
	 * Handles the head element of the response.
	 * </p>
	 * <p>
	 * This implementation performs no action.
	 * </p>
	 * 
	 * @param head
	 */
	protected void handleHead(JSONObject head) {
		_log.debug("Handling head {}", head);
	}

	/**
	 * Iterates over all result objects.
	 * 
	 * @param results
	 * @param resultList
	 */
	protected void handleResults(JSONObject results,
			LinkedList<Map<String, VariableBinding>> resultList) {
		_log.debug("Handling results {}", results);
		final JSONArray bindingsArray = (JSONArray) results.get(KEY_BINDINGS);
		final Iterator<Object> bindingsIt = bindingsArray.iterator();
		while (bindingsIt.hasNext()) {
			handleBinding((JSONObject) bindingsIt.next(), resultList);
		}
	}

	/**
	 * Executes the corresponding method for the bindings via Java reflection.
	 * 
	 * @param binding
	 * @param resultList
	 */
	protected void handleBinding(JSONObject binding,
			LinkedList<Map<String, VariableBinding>> resultList) {
		_log.debug("Handling binding {}", binding);

		for (Entry<String, Object> bindingEntry : binding.entrySet()) {
			final JSONObject bindingValue = (JSONObject) bindingEntry
					.getValue();
			final String variableName = bindingEntry.getKey();
			final String type = (String) bindingValue.get(KEY_TYPE);
			final String datatype = (String) bindingValue.get(KEY_DATATYPE);
			final Object value = (String) bindingValue.get(KEY_VALUE);
			final Map<String, VariableBinding> result = new HashMap<String, VariableBinding>();
			Object[] parameters = { variableName, value, datatype, result };
			resultList.add(result);

			try {
				TYPES_MAP.get(type).invoke(this, parameters);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	/**
	 * Called by {@link #handleBinding(JSONObject)} via Java reflection.
	 * 
	 * @param value
	 * @param datatype
	 */
	@SuppressWarnings("unused")
	private void _handleUri(String key, String value, String datatype,
			Map<String, VariableBinding> result) {
		result.put(key, new VariableBinding(key, value, TYPE_URI));
	}

	/**
	 * Called by {@link #handleBinding(JSONObject)} via Java reflection.
	 * 
	 * @param value
	 * @param datatype
	 */
	@SuppressWarnings("unused")
	private void _handleLiteral(String key, String value, String datatype,
			Map<String, VariableBinding> result) {
		result.put(key, new VariableBinding(key, value,
				datatype == null ? TYPE_LITERAL : datatype));
	}

	/**
	 * Called by {@link #handleBinding(JSONObject)} via Java reflection.
	 * 
	 * @param value
	 * @param datatype
	 */
	@SuppressWarnings("unused")
	private void _handleBNode(String key, String value, String datatype,
			Map<String, VariableBinding> result) {
		result.put(key, new VariableBinding(key, value, TYPE_BNODE));

	}
	//
	//
	//

}
