package ch.uzh.ifi.ddis.sparqlightning;

/*
 * #%L
 * SPARQLightning
 * %%
 * Copyright (C) 2013 University of Zurich, Department of Informatics
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.apache.http.Consts;
import org.apache.http.HttpHeaders;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Thomas Scharrenbach
 * @version 0.0.1
 * @since 0.0.1
 * 
 */
public class PostUrlEncodedRequestFactory implements RequestFactory {

	private static final Logger _log = LoggerFactory
			.getLogger(PostUrlEncodedRequestFactory.class);

	//
	//
	//

	public HttpPost createQueryRequest(String endpoint, String defaultGraphUri,
			String namedGraphUri, String query, List<String> acceptedMediaTypes)
			throws URISyntaxException, ClientProtocolException, IOException {

		final URIBuilder builder = new URIBuilder(endpoint);

		final HttpPost request = new HttpPost(builder.build());

		// Only now add parameters, since they have to be contained in the
		// message body rather than encoded as parameters.
		builder.setParameter(PARAM_QUERY, query);
		if (defaultGraphUri != null) {
			builder.setParameter(PARAM_DEFAULT_GRAPH_URI, defaultGraphUri);
		}
		if (namedGraphUri != null) {
			builder.setParameter(PARAM_NAMED_GRAPH_URI, namedGraphUri);
		}

		final StringEntity body = new StringEntity(URLEncodedUtils.format(
				builder.getQueryParams(), Consts.UTF_8), ContentType.create(
				CONTENT_TYPE_FORM_URL_ENCODED, Consts.UTF_8));

		request.addHeader(HttpHeaders.ACCEPT, ACCEPT_TYPE_TURTLE);
		request.addHeader(HttpHeaders.ACCEPT, "charset=utf-8");

		request.setEntity(body);

		_log.debug("Created request \"{}\"", request);

		return request;

	}

	public HttpRequestBase createUpdateRequest(String endpoint,
			String usingGraphUri, String usingNamedGraphUri, String update)
			throws URISyntaxException, ClientProtocolException, IOException {
		final URIBuilder builder = new URIBuilder(endpoint);

		final HttpPost request = new HttpPost(builder.build());
		// Only now add parameters, since they have to be contained in the
		// message body rather than encoded as parameters.
		builder.setParameter(PARAM_UPDATE, update);
		if (usingGraphUri != null) {
			builder.setParameter(PARAM_USING_GRAPH_URI, usingGraphUri);
		}
		if (usingNamedGraphUri != null) {
			builder.setParameter(PARAM_USING_NAMED_GRAPH_URI,
					usingNamedGraphUri);
		}
		final String bodyString = URLEncodedUtils.format(
				builder.getQueryParams(), Consts.UTF_8);
		_log.debug("Created request body: {}", bodyString);
		final StringEntity body = new StringEntity(bodyString,
				ContentType.create(CONTENT_TYPE_FORM_URL_ENCODED));

		request.setEntity(body);

		// request.setHeader(HttpHeaders.CONTENT_TYPE,
		// CONTENT_TYPE_FORM_URL_ENCODED);
		request.setHeader(HttpHeaders.ACCEPT, "text/plain");
		// request.addHeader(HttpHeaders.ACCEPT, "charset=utf-8");

		_log.debug("Created request \"{}\"", request);

		return request;
	}
}
