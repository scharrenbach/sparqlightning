package ch.uzh.ifi.ddis.sparqlightning;

/*
 * #%L
 * SPARQLightning
 * %%
 * Copyright (C) 2013 University of Zurich, Department of Informatics
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.jena.fuseki.FusekiCmd;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

/**
 * 
 * @author Thomas Scharrenbach
 * @version 0.0.1
 * @since 0.0.1
 * 
 */
public abstract class TestRequestFactory {

	private static final Logger _log = LoggerFactory
			.getLogger(TestRequestFactory.class);

	private Thread _fusekiThread;

	private Process _fusekiProcess;

	/**
	 * Taken from
	 * http://stackoverflow.com/questions/636367/executing-a-java-application
	 * -in-a-separate-process
	 * 
	 * @param clazz
	 * @param args
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static ProcessBuilder exec(Class<?> clazz, String[] args)
			throws IOException, InterruptedException {
		String javaHome = System.getProperty("java.home");
		String javaBin = javaHome + File.separator + "bin" + File.separator
				+ "java";
		String classpath = System.getProperty("java.class.path");
		String className = clazz.getCanonicalName();

		String[] processArgs = new String[4 + args.length];
		processArgs[0] = javaBin;
		processArgs[1] = "-cp";
		processArgs[2] = classpath;
		processArgs[3] = className;
		for (int i = 0; i < args.length; ++i) {
			processArgs[i + 4] = args[i];
		}

		return new ProcessBuilder(processArgs);

	}
	
	@BeforeSuite
	public void startFuseki() {

		try {
			_fusekiThread = new Thread() {
				@Override
				public void run() {
					super.run();
					final String[] args = { "--update", "--mem", "/test" };
					try {
						final ProcessBuilder builder = exec(FusekiCmd.class,
								args);
						setFusekiProcess(builder.start());
					} catch (Exception e) {
						_log.error("Error starting Fuseki!", e);
					}
				}

				@Override
				public void interrupt() {
					getFusekiProcess().destroy();
//					_log.info("Fuseki stopped with exit code {}",
//							getFusekiProcess().exitValue());
					super.interrupt();
				}
			};
			_fusekiThread.start();
			Thread.sleep(10000);
		} catch (Exception e) {
			_log.error("Error!", e);
			assert false;
		}
		assert true;
	}

	/**
	 * @param endpoint
	 *            "http://dbpedia.org/sparql"
	 * @param query
	 *            "SELECT * WHERE{ ?s ?p ?o } LIMIT 10"
	 */
	@Test
	@Parameters({ "requestFactoryClass", "endpoint", "query" })
	public void testQuery(String requestFactoryClass, String endpoint,
			String query) {

		try {
			final HttpRequestBase request = createRequest(requestFactoryClass,
					endpoint, query);

			final HttpClient httpclient = new DefaultHttpClient();
			final HttpResponse response = httpclient.execute(request);
			evaluate(response);
		} catch (Exception e) {
			_log.error("Error in test: ", e);
			assert false;
		}
	}

	protected abstract HttpRequestBase createRequest(
			String requestFactoryClass, String endpoint, String query)
			throws Exception;

	private void evaluate(HttpResponse response) throws Exception {
		_log.info("Response code: {}", response.getStatusLine());

		// Status may be 2xx or 3xx.
		assert response.getStatusLine().getStatusCode() >= 200
				&& response.getStatusLine().getStatusCode() < 400;

		if (response.getEntity() != null) {
			final InputStream in = response.getEntity().getContent();
			final StringBuffer responseString = new StringBuffer();
			int size = 0;
			byte[] buffer = new byte[1024];
			while ((size = in.read(buffer)) != -1) {
				responseString.append(new String(buffer, 0, size));
			}
			in.close();
			_log.info("Response: {}", responseString);
		}
	}

	@AfterSuite
	public void stopFuseki() {
		_fusekiThread.interrupt();
	}

	public Process getFusekiProcess() {
		return _fusekiProcess;
	}

	public void setFusekiProcess(Process fusekiProcess) {
		_fusekiProcess = fusekiProcess;
	}
}
