package ch.uzh.ifi.ddis.sparqlightning;

/*
 * #%L
 * SPARQLightning
 * %%
 * Copyright (C) 2013 University of Zurich, Department of Informatics
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class TestJSONResultSet {

	private static final Logger _log = org.slf4j.LoggerFactory
			.getLogger(TestJSONResultSet.class);

	@Test
	@Parameters({ "jsonFile" })
	public void test(
			@Optional(value = "src/test/resources/output.srx") String jsonFile) {
		FileInputStream in = null;
		try {
			in = new FileInputStream(jsonFile);

			// Create the parser.
			final JSONResultSetParser resultSet = new JSONResultSetParserImpl();

			// Parse the result set.
			final List<Map<String, VariableBinding>> result = resultSet
					.parse(in);

			// Print all results to the log.
			for (Map<String, VariableBinding> row : result) {
				_log.info("Result: {}", row);
			}
		} catch (Exception e) {
			_log.error("Error testing {}!", TestJSONResultSet.class, e);
			assert false;
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					_log.error("Error closing input stream!", e);
					assert false;
				}
			}
		}
	}
}
